// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/**************
 **The BASICS**
 **************/
/*************************************************************************

                      CONSTANTS AND VARIABLES
*************************************************************************/

//Declaring Constants - let
//Declaring Variables - var

//e.g. 
let maximumNumberOfLoginAttemps = 10
var currentLoginAttempt = 0

/*
 “NOTE

  If a stored value in your code is not going to change, always declare it as a constant with the let keyword. Use variables only for storing values that need to be able to change.”
*/

/*************************************************************************

                        TYPE ANNOTATIONS
*************************************************************************/

//e.g.
var welcomeMessage: String //colon means 'of type' -- welcomeMessage of type String
welcomeMessage = "Hello"

/*************************************************************************

                    NAMING CONSTANTS AND VARIABLE
*************************************************************************/

//You can use almost any character for cons and var names including unicode

//e.g.
let π = 3.14159
let 你好 = "你好世界"
let 🐶🐮 = "dogcow"

//Changing the stored value of a variable
var friendlyWelcome = "Hello"
friendlyWelcome = "Bonjour"

//Value of constant cannot be change
let languageName = "Swift"
//languageName = "Swift++" //compile time error

/*************************************************************************

                    PRINTING CONSTANTS AND VARIABLES
*************************************************************************/

//by Using the println function, println values prints any String values you pass to it
println(friendlyWelcome)

//using string interpolation
println("The current value of friendlyWelcome is \(friendlyWelcome)")


/*************************************************************************

                            SEMI-COLONS
*************************************************************************/

//Swift does not require write a semicolon after each statement.
//Semicolons are required when writing multiple separate statements on a single line
let cat = "🐱"; println(cat)

/*************************************************************************

                            INTEGERS
*************************************************************************/

//Integers are whole numbers with no fractial component

//Integers are either signed(positive,zero or negative)
//or unsigned(positive or zero)

//8-bit unsigned integers - UInt8
//32-bit signed integers - Int32

//**INTEGER BOUNDS
    //Accessing min and max value of integer type
//e.g.
let minValue = UInt8.min //0 of type UInt8
let maxValue = UInt8.max //255

//**INT 
    //On a 32-bit platform, Int is the same size as Int32
    //On a 32-bit platform, Int is the same size as Int32

//**INT
    //On a 32-bit platform, UInt is the same size as Int32
    //On a 32-bit platform, UInt is the same size as Int32

/*************************************************************************

                    FLOATING-POINT NUMBERS
*************************************************************************/

//Swift provide two signed floating-point number types:

//1. Double - represents 64 bit floating point number for large values
//2. Float - represents a 32-bit floating point number. Use if do not require 64 bit precision.

//Double - atleast 15 decimal digits precision
//Float - as little as 6 decimal digit precision

/*************************************************************************

                    TYPE SAFETY AND TYPE INFERENCE
*************************************************************************/

//If you assign a value to a constant or variable without setting it's type Swift automatically infers to the constant/variable type to be the type of the value 
//e.g
let meaningfulOfLife = 42
// meaningful of life is inferred to be of type Int

//e.g.
let pi = 3.14159
//pi is inferred of type Double
//Swift always chooses Double rather than Float when inferring floating-point numbers

//if interger and floating point literals are combined in an expression -- Double will be inferred from the context
//e.g.
let anotherPi = 3 + 0.14159

/*************************************************************************

                            NUMERIC LITERALS
*************************************************************************/

//Numeric literals can be written as:

    //1. decimal number, with no prefix
    //2. binary number, with a 0b prefix
    //3. An octal number, with a 0a prefix
    //4. A hexadecimal number, with a 0x prefix

//e.g.
let decimalInteger = 17
let binaryInteger = 0b10001 // 17 in binary Notation
let octalInteger = 0o21 // 17 in octal
let hexadecimalInteger = 0x11

//Floating point exponents
//e.g.
//decimal
let expFloat = 1.25e2 //reads as 1.25 x 10^2
//hex
let hexExpFloat = 0xFp2 //reads as 15 x 2^2

//Extra Formatting for Readability
//by adding extra zero
let paddedDouble = 000123.456
//by adding underscores
let oneMillion = 1_000_000
let justOverOneMillion = 1_000_000.000_000_1

/*************************************************************************

                        NUMERIC TYPE CONVERSION
*************************************************************************/

//Use the int type for all general-purpose integer constants and variable in your code even if it is negative. -Interoperable and match the inferred type

//-------------------------INTEGER TYPE CONVERSION------------------------

//signed Int8 = -128 to 128
//unsigned UInt8 = 0 to 255

//e.g.
//let cannotBeNegative: UInt8 = -1 //Error - Cannot be negative
//let tooBig: Int8 = Int8.max + 1 //Error - Cannot be larger

//When converting the numeric type you must specify the type of the numeric to the value your converting

//e.g.
let twoThousand: UInt16 = 2_000
let one: UInt8 = 1
let twoThousandAndOne = twoThousand + UInt16(one) //one is converted to UInt16 to enable addition

//SomeType(ofInitialValue) is the default way to call the initializer of a swift type and pass in an initial value.
//It has to be the type for which SomeType or UInt16 provides an initializer


//---------INTEGER AND FLOATING POINT CONVERSION---------------------------

//Conversion between integer and floating-point numeric types must be made explicit:
//e.g.
let three = 3
let pointOneFourOneFiveNine = 0.14159
let pi2 = Double(three) + pointOneFourOneFiveNine //pi equals 3.14159, and is inferred of type Double

//You must balance the conversion in a math operation. They must be on the same type.

//float to Int
//e.g.
let integerPi = Int(pi2) //FLoating point values are always truncated when you convert a floating point to int. 4.75 = 4, -3.7 = -3

/*
“NOTE

“The rules for combining numeric constants and variables are different from the rules for numeric literals. The literal value 3 can be added directly to the literal value 0.14159, because number literals do not have an explicit type in and of themselves. Their type is inferred only at the point that they are evaluated by the compiler.”

*/

/*************************************************************************

                            TYPE ALIASES
*************************************************************************/

//Type Aliases defines an alternative name for an existing type using the typealias keyword

//Type Aliases are useful when working with data of a specific size

//e.g.
typealias AudioSample = UInt16

var maxAmplitudeFound = AudioSample.min //now 0 or UInt16.min

/*************************************************************************

                            BOOLEAN
*************************************************************************/

//Swift -> Bool

//You don't need to declare constant/variables as Bool. It's always true or false

//e.g.
let orangesAreOrange = true
let turnipsAreDelicious = false

if turnipsAreDelicious {
    println("Mmm, tasty turnips")
} else {
    println("Eww, turnips are horrible")
}


/*************************************************************************

                            TUPLES
*************************************************************************/

//Tupples group multiple values into a single compound
//Tuples can be of any type -- can be (Int, Int, String) or (Bool, String, Int)

let http404Error = (404, "Not found") // a tuple of type (Int, String)

    //DECOMPOSING TUPLES
let (statusCode, statusMessage) = http404Error
println("The status code is \(statusCode)")
println("The status message is \(statusMessage)")

//**Use underscore(_) if you want to ignore parts of tuples

let (justTheStatusCode, _) = http404Error
println("The status code is \(justTheStatusCode)")

//**You can access tuples by using it's index
println("The status code is \(http404Error.0)")
println("The status code is \(http404Error.1)")

//**You can name tuples and access them by the named properties
let http200Status = (statusCode: 200, description: "Ok")
println("The status code is \(http200Status.statusCode)")
println("The status message is \(http200Status.description)")

/*
“NOTE

Tuples are useful for temporary groups of related values. They are not suited to the creation of complex data structures. If your data structure is likely to persist beyond a temporary scope, model it as a class or structure, rather than as a tuple. For more information, see”

*/


/*************************************************************************

                            OPTIONALS
*************************************************************************/

//Use optionals in situations where a value may be absent.
//An optional says there is a value and equals x or there isn't a value at all

//e.g
let possibleNumber = "123" //If possibleNumber = "mm" converted number will return nil
let convertedNumber = possibleNumber.toInt()

//optional is used if the expected return type is not met. It returns an optional value if the criterias are incorrect for the method.
//it return a valid value or none at all

//**FORCE UNWRAPPING
//You can check whether the optional does contain value by using condition statement

if convertedNumber != nil {
    println("\(possibleNumber) has an interger Value of \(convertedNumber!)") //adding exclamation(!) to the constant or variable lets you access or unwrap the value
} else {
    println("\(possibleNumber) could not be converted to an integer")
}

/*
“NOTE

Trying to use ! to access a non-existent optional value triggers a runtime error. Always make sure that an optional contains a non-nil value before using ! to force-unwrap its value.”

 */


//--->OPTIONAL BINDING
//Used to find out whether an optional contains a value -- if it contains make that temporary constant or variable

//optional binding
if let actualNumber = possibleNumber.toInt() { //returns true if the optional int contains a value and set the value to the constant named "actualNumber". So no need to use the ! suffix
    println("\(possibleNumber) has an integer value of \(actualNumber)")
} else {
    println("\(possibleNumber) could not be converted to Integer")
}

//--->NIL
//You can set an optional to a variable to a valueless state by assigning nil

//e.g.
var serverResponseCode: Int? = 404 //serverResponse contains an actual Int value of 404
serverResponseCode = nil

var surveyAnswer: String? //return nil if there is no value specified
surveyAnswer = "sample"

/*
“NOTE

nil cannot be used with non-optional constants and variables. If a constant or variable in your code needs to be able to cope with the absence of a value under certain conditions, always declare it as an optional value of the appropriate type.”

“Swift’s nil is not the same as nil in Objective-C. In Objective-C, nil is a pointer to a non-existent object. In Swift, nil is not a pointer—it is the absence of a value of a certain type. Optionals of any type can be set to nil, not just object types.”

*/

//--->IMPLICITLY UNWRAPPED OPTIONALS

//Instead of using exclamation mark all the time when accessing optional constant/variable, you can place an exclamation mark after the optional's type after you declare it.

//e.g.
let possibleString: String? = "An optional string"
println(possibleString!) //requires an exclamation mark to access it's values

//no exclamation mark is needed to access it's value
let assumedString: String! = "An implicitly unwrapped optional string."
println(assumedString)

let noValueString: String? //ignore the warning -- this will return an error unless you put a value in it.
//println("IMPLICIT NIL STRING OPTIONA \(noValueString!)") //--will return error when trying to unwrapped optionals with no value

//checking an implicitly unwrapped values
if assumedString != nil {
    println(assumedString)
}

//Implicitly unwrapped optional with optional binding
//e.g.
if let definiteString = assumedString {
    println(definiteString)
}

/*
Implicitly unwrapped optionals should not be used when there is a possibility of a variable becoming nil at a later point. Always use a normal optional type if you need to check for a nil value during the lifetime of a variable.”
*/


/**************************************************************************
ASSERTIONS
**************************************************************************/

/*If code condition evaluates to true, code execution continues as usual
If code condition evaluates to false, code execution ends, app is terminated
In debug mode, you can see where the state occured
*/

//e.g.
let age = -3
//assert(age >= 0, "A person's age cannot be less than zero") //this causes the assertion to trigger, because age is not >= 0

//assetion message cannot use String interpolation

//Ommited assertion
//e.g.
//assert(age >= 0)

/*--->WHEN TO USE ASSERTIONS
1. An integer subscript index is passed to a custom subscript implementation, but the scubscript index value could be to low or too high
2. A value is passed to a function, but an invalid value means that the function cannot fullfill it's task.
3. An optional value is currently nil, but a non-nil value is essential for subsequent code to execute successfully.
*/


/*******************
**BASIC OPERATORS**
*******************/

/*
= -  assignment operator
== - equal to operator
a..b or a...b - range operators
*/

/**************************************************************************
TERMINOLOGY
**************************************************************************/
/*
Operators are
1. Unary
e.g. -a, !b, i++
2. Binary
e.g. (2 + 3), (6 / 3)
3. Ternary
e.g. (a ? b : c) - only ternary conditions
*/

/**************************************************************************
ASSIGNMENT OPERATOR
**************************************************************************/

//e.g.
let b = 10
var a = 5
a = b

//***TUPLE
//e.g.
let (x, y) = (1, 2) //x = 1, y = 2


/**************************************************************************
ARITHMETHIC OPERATORS
**************************************************************************/

1 + 2 //Addition
5 - 3 //Substraction
2 * 3 //Multiplication
10.0 / 2.5 //Division

//Character
//e.g.
let dog: Character = "🐶"
let cow: Character = "🐮"
let dogCow = dog + cow


/**************************************************************************
REMAINDER OPERATOR
**************************************************************************/

9 % 4 //equals to 1

/**************************************************************************
FLOATING-POINT REMAINDER CALCULATIONS
**************************************************************************/

8 % 2.5

/**************************************************************************
INCREMENT AND DECREMENT OPERATORS
**************************************************************************/

/*
RULES:
1. If the operator is written before the variable, it increments the variable before returning its value.
2. If the operator is written after the variable, it increments the variable after returning its value

*/

//e.g.
var m = 0
let n = ++m //1
let o = m++ //2

/**************************************************************************
UNARY MINUS OPERATOR
**************************************************************************/

//e.g
let minusThree = -three
let plusThree = -minusThree

/**************************************************************************
UNARY PLUS OPERATOR
**************************************************************************/

//just returns the value it operates one
let minusSix = -6
let alsoMinusSix = +minusSix //-6

/**************************************************************************
COMPOUND ASSIGNMENT OPERATORS
**************************************************************************/

//addition assignment
//e.g.
var j = 1
j += 2 //a is now 3

/**************************************************************************
COMPARISON OPERATORS
**************************************************************************/

/*
Comparison Operators
Equal to (a == b)
Not equal to (a != b)
Greater than (a > b)
Less than (a < b)
Greater than or equal to (a >= b)
Less than or equal to (a <= b)
two identity (===, !==) test whether to objects references both instance
*/

//e.g

let name = "world"

if name == "world" {
    println("hello world")
} else {
    println("Im sorry \(name), but I don't recognize you")
}

/**************************************************************************
TERNARY CONDITIONAL OPERATOR
**************************************************************************/

//Regular condition:
let question: String = "Is this swift?"
var answer = false
if question == "Is this swift?" {
    answer = true
} else {
    answer = false
}

//Ternary
answer = (question == "Is this Objective C?") ? true : false


//e.g - Calculates the pixel height for a table row. The row height should be  50 pixels  taller than the content height if the row has header, and 20 pixels taller if the row doesn't have a header

let contentHeight = 40
let hasHeader = true
let rowHeight = contentHeight + (hasHeader ? 50 : 20)

//shorthand version of the code
var rowHeight2 = contentHeight
if hasHeader {
    rowHeight2 = rowHeight2 + 50
} else {
    rowHeight2 = rowHeight2 + 20
}

//NOTE: Avoid combining multiple instances of the ternary conditional operator into one compound statement.

/**************************************************************************
RANGE OPERATORS
**************************************************************************/

/*
---->Closed Range Operators
The closed range operator (a...b) defines a range that runs from a to b, and includes the values a and b.

*/

//e.g.
for index in 1...5 {
    println("\(index) times 5 is \(index * 5)") //index starts at 1 and ends at 5. This loop will run 5 times.
}

/*
---->Half-Closed Range Operators
The closed range operator (a..<b) defines a range that runs from a to b, but does not include b. It is said to be half-closed because it contains it's first value but not its final value

Best used in: zero-based list such as arrays
*/

//e.g
let names = ["Anna", "Alex", "Brian", "Jack"]
for i in 0..<names.count {
    println("Person \(i + 1) is called \(names[i])")
}

/**************************************************************************
LOGICAL OPERATORS
**************************************************************************/

/*
Modify or combine the Boolean logic values.

Logical NOT (!a)
Logical AND (a && b)

*/

//---> Logical NOT Operator --inverts a boolean value
//e.g.
let allowedEntry = false
if !allowedEntry { //reads as "allowed entry is false"
    println("ACCESS DENIED")
}
//prints ACCESS DENIED

/*
----->Logical AND Operator

Short circuit evaluation - if the first value is false the second value won't even be evaluated.
*/
//e.g.
let enteredDoorCode = true
let passedRetinaScan = false
if enteredDoorCode && passedRetinaScan {
    println("Welcome!")
} else {
    println("Access Denied")
}

/*
----->Logical OR Operator

Uses short circuit evaluation. If the one of the two conditions is true return true.
*/

//e.g.
let hasDoorKey = false
let knowsOverridePassword = true
if hasDoorKey || knowsOverridePassword {
    println("Welcome!")
} else {
    println("ACCESS DENIED")
}
//prints Welcome

/*
----->Combining Logical Operator
//It's readable to add Explicit Parentheses for combined logical operator
*/
//e.g.
if (enteredDoorCode && passedRetinaScan) || hasDoorKey || knowsOverridePassword {
    println("Welcome")
} else {
    println("ACCESS DENIED")
}
//prints "Welcome"


/**************************
**Strings and Characters**
**************************/

/*
Mutablitity and Concatenation has been simple
Unicode can be used
*/

/************************************************************************
                            Strings and Characters
**************************************************************************/

/*
String literals is fixed sequence of textual characters surrounded by a pair of double quotes. ("")

Examples:
\0 - null character
\\ - backslash
\t - horizontal tab
\n - line feed
\r - carriage return
\' - single quote
\xnn - single byte unicode scalars
\unnnn - two byte unicode scalars
\Unnnnnnnn - four-byte unicode scalars
*/

//e.g.
let someString = "Some string literal value"
let wiseWords = "\"Imagination is more important than knowledge\" - Einstein"
let dollarSign = "\u{24}"
let blackheart = "\u{2665}"
let sparklingHeart = "\u{1F496}"

/************************************************************************
                        Initializing an Empty String
**************************************************************************/

//e.g
//empty string literal
var emptyString = ""

//initializer syntax
var anotherEmptyString = String()
var s: String //nil


// these two strings are both empty, and are equivalent to each other

//Check if string value is empty
if emptyString.isEmpty {
    println("Nothing to see here")
}
//prints nothing to see here

/************************************************************************
                            String Mutability
**************************************************************************/

var variableString = "Horse"
variableString += " and carriage"
//variableString is now "Horse and carriage"

let constantString = "Highlander"
//constantString += " and another Highlander" //compile time error

/************************************************************************
                        Strings Are Value Types
*************************************************************************/

/*
string type is value type. When you pass a string value to a function or a constant/variable, a new string value is created. Unlike Objective C you just get the reference to that String

NOTE: Pass by Value
Pass by Reference
*/

/************************************************************************
                        Working with Characters
*************************************************************************/
/*
Swift's string type represents a collection of Character.
You can access the individual Character values in a String by iterating over the string.
*/
//e.g.
for character in "Dog!🐶" {
    println(character)
}
//D
//o
//g
//!
//🐶

//Standalone character constant
let yenSign: Character = "¥"

/************************************************************************
                        Counting Characters
*************************************************************************/

/*
countElements - retrieve a count of characters in string
*/

let unusualMenagerie = "Koala, Snail, Penguin, Dromedary"
println("usualMenagerie has \(countElements(unusualMenagerie)) characters")

/************************************************************************
                    Concatenating Strings and Characters
*************************************************************************/

/*
    String and character values can be added together or concatenated to create a new String value
 */

let string1 = "hello"
let string2 = " there"
let character1: Character = "!"
let character2: Character = "?"

let stringPlusCharacter = string1 + character1
let stringPlusString = string1 + string2
let characterPlusString = character1 + string1
let characterPlusCharacter = character1 + character2

//Using += 
var instruction = "look over"
instruction += string2

var welcome = "good morning"
welcome += character1

//You can't append a string or character to an existing character -- character must only contain a single character

/************************************************************************
                    String Interpolation
*************************************************************************/

/*
    String Interpolation is a way to construct a new string value from a mix of constants, variables, literals, and expressions.
 */

let multiplier = 3
let message = "\(multiplier) times 2.5 is \(Double(multiplier) * 2.5)"
//message is "3 time 2.5 is 7.5"

/************************************************************************
                        Comparing String
*************************************************************************/

/*
 ----->String Equality
        If two string values are considered equal if they contain exactly the same character in the same order
 */

let quotation = "We're a lot alike, you and I."
let sameQuotation = "We're a lot alike, you and I."

if quotation == sameQuotation {
    println("This two string are considered equal")
}

/*
 ----->Prefix and Suffix Equality
        Check whether a string has particular string prefix or suffix.

        hasPrefix & hasSuffix - returns bool
 */

let mckein = "Mckein"
let apples = "apples"
let LoveFruits = "Mckein loves apples"

if LoveFruits.hasPrefix(mckein) {
    println("\(mckein) love fruits")
}

if LoveFruits.hasSuffix(apples) {
    println("They love \(apples)")
}


/*
 ----->Uppercase and Lowercase Strings
 */

let normal = "Could you help me please"
let shouty = normal.uppercaseString
let whispered = normal.lowercaseString


if shouty == whispered {
    println("They are equal")
} else {
    println("They are not equal")
}

/************************************************************************
                            UNICODE
*************************************************************************/


/*
  -----> UTF8
        -a collection of unsigned 8-bit(UInt8) values
        -gets the value by specifying codeUnit in the iterator
 */

let dogString = "Dog!🐶"

for codeUnit in dogString.utf8 {
    print("\(codeUnit) ")
}
//68 111 103 33 240 159 144 182
//D   o   g   !    DOG FACE

/*
  -----> UTF16
        -a collection of unsigned 16-bit(UInt16) values
        -gets the value by specifying codeUnit in the iterator
*/

for codeUnit in dogString.utf16 {
    print("\(codeUnit) ")
}
//68 111 103 33 55357 56374
//D   o    g  !  DOG FACE

/*
   -----> Unicode Scalar
        -a collection of 21-bit values
        -get character - scalar
        -get value - scalar.value
*/

for scalar in dogString.unicodeScalars {
    print("\(scalar.value) ")
}
//68 111 103 33 128054


/**************************
 *****Collection Types*****
 **************************/

/*
    Array - ordered list
    Dictionaries - key value
 */

/************************************************************************
                            ARRAYS
*************************************************************************/

/*
    -Swift arrays are specific about the kind of values they store.
    -Declared type through explicit type annotation or type inference
 */

/*--------------->Array Type Shorthand Syntax

        Two types to create an array
    1. Full form
        Array<SomeType>
    2. Shorthand form - preferred to use
        var x: [SomeType]
 */

/*-------------------->Array Literals
        array literal as a list of values seperated by commas, surrounded by square brackets

 */
//e.g.
var shoppingList: [String] = ["Eggs", "Milks"]

//Type Inferred Array
//e.g
var shoppingList2 = ["Eggs", "Milk"]

/*
----------> Accessing and Modifying an Array
Properties:
1. count - returns the number of items in array
2. isEmpty - (Boolean) check whether the count property = 0

Method:
1. append - add an item to the end of array
*/
//e.g.
//1
println("The shopping list contatins \(shoppingList.count)")

//2
if shoppingList.isEmpty {
    println("The shopping list is empty") //prints this one
} else {
    println("The shopping list is not empty")
}

//append
shoppingList.append("Flour")
//or
shoppingList += ["Baking Powder"] //it's better to use append

//appendding multiple items
shoppingList += ["Chocolate Spread", "Cheese", "Butter"] //additional operator

/*
--------> Retrieving an array value

You can retrieve a value from the array by using subscript syntax
*/
//e.g.
var firstItem = shoppingList[0]

/*
---------> Update an existing value

You can use subscript syntax to update/change an existing value at a given index
*/
//e.g.
shoppingList[0] = "Six Eggs"

/*
-------> Change a range of values

You can use subscript syntax to change a range of values
*/
//e.g.
shoppingList[4...6] = ["Bananas", "Apple"] //replace the array 4,5,6 with "Bananas" and "Apple"
shoppingList

//------> Inserting values at a specific index

shoppingList.insert("Maple Syrup", atIndex: 0) //shopping list now contains 7 items, is not at the first item
shoppingList

/*
NOTE:
Before changing values or updating the range it's better to check the count of the array first.
*/

//------> Removing values at a specific index

shoppingList.removeAtIndex(0) //Maple Syrup is removed from the array
let apples3 = shoppingList.removeLast() // removed Apple

/************************************************************************
                            ITERATING Over an ARRAY
*************************************************************************/

//------> For-in Loop
for item in shoppingList {
    println(item)
}

//Use enumerate function to get index and value
for (index, value) in enumerate(shoppingList) {
    println("Item \(index + 1): \(value)") // get array index and value
}

/************************************************************************
                            Creating and Initializing
*************************************************************************/

//-->Create empty array without using initializer syntax
//e.g.
var someInts = [Int]() //initializer
println("someInts is of type Int[] with \(someInts.count)")
someInts.append(3) //aray has now 1 values
someInts = [] //array is now empty

//-->Setting the size of array and value
var threeDouble = [Double](count: 3, repeatedValue: 0.0)
// threeDouble is type [Double], and equals [0.0, 0.0, 0.0]

var anotherThreeDouble = Array(count: 3, repeatedValue: 2.5) //Type inferred array
// anotherThreeDouble is inferred as [Double], and equals [2.5, 2.5, 2.5]

//--> Adding arrays together of compatible type
var sixDoubles = threeDouble + anotherThreeDouble // by using the + operator

/************************************************************************
                                Dictionaries
*************************************************************************/

/*
    Swift dictionaries are always made clear
    
    Dicionary<KeyType, ValueType>
        KeyType must be hashable, (String, Int, Double, Bool) hashable by defualt
        Enumeration member values are also hashable
 */


/************************************************************************
                            Dictionary Literals
*************************************************************************/

/*
    Same syntax as of the array literal -- in dictionary you just use the key-value pair
 */

var airports: Dictionary<String, String> = ["TYO": "Tokyo", "DUB": "Dublin"] //airports of dictionary type String, String

//Type inferred
var airports2 = ["TYO": "Tokyo", "DUB": "Dubline"]

/************************************************************************
                    Accessing and Modifying a Dictionary
*************************************************************************/

/* 
    Properties:
        1. count - number of items
        2.
 */

println("The dictionary of airports contain \(airports.count)") //2 items

//-->Adding using SUBSCRIPT SYNTAX
airports["LHR"] = "London"
airports

//-->Modifyign using SUBSCRIPT SYNTAX
airports["LHR"] = "London Heathrow"
//or using the updateValue(forKey:) method -- returns old value before update, check whether an updtae took place
//return an optional value
if let oldValue = airports.updateValue("Dublin International", forKey: "DUB") {
    println("The old value for Dub was \(oldValue)")
}

//-->Retrieving using SUBSCRIPT SYNTAX
if let airportName = airports["DUB"] { //optional value
    println("The name of the airport is \(airportName).")
    airportName
} else {
    println("The airport is not in the airports dictionary")
}

//-->Removing a key value pair using SUBSCRIPT SYNTAX
airports["APL"] = "Apple International"
airports["APL"] = nil //APL has now been removed to the dict

//-->Iterating over a dictionary
//Each item in the dictionary is returned as (key, value) tuple
for (airportCode, airportName) in airports {
    println("\(airportCode): \(airportName)")
    airportCode
    airportName
}

//-->Retrieving keys and values seperately
for airportCode in airports.keys {
    println("Airport code: \(airportCode)")
    airportCode
}

for airportName in airports.values {
    println("Airport Name \(airportName)")
    airportName
}

//Initialize a new array of keys and values
let airportCodes = Array(airports.keys)
let airportNames = Array(airports.values)

/*
    Swift's dictionary is an UNORDERED COLLECTION. The order in which keys, values, and key-value paires are retrieved when iterating over a dictionary is not specified.
 */

/************************************************************************
                    Creating an Empty Dictionary
*************************************************************************/

var namesOfInteger = Dictionary<Int, String>() //same as array [Int]()
//e.g.
namesOfInteger[0] = "Zero" // [key] = value
namesOfInteger[1] = "One"

//---->Emptying a dictionary
namesOfInteger = [:]

/*
    Note:
        Array and Dictionaries are implemented as generic collections
 */

/************************************************************************
                    Mutability of Collections
*************************************************************************/

/*
    If the created arrays and dictionary are assigned into a variable, then the collection that is created will MUTABLE.

    It is a good practice to create immutable collections in all cases where the collection size does not need to change.
    Doing so enables the Swift compiler to optimize the performance of the collections you create.

 */


/******************
 ***Control Flow***
 ******************/

/************************************************************************
                        For Loops
*************************************************************************/

/* 
    1. for-in - perform a set of statement in range, sequence, collection, or progression
    2. for-condition-increment - perform set of statement until condition is met,typically by incrementing a counter each time a loop ends
 */

// ------> For-in

//iterates from 1 to 5 using closed range operator(...), index is a constant who is automatically set at the start of each iteration.
for index in 1...5 {
    println("\(index) times 5 is \(index * 5)")
}

//When you don't need each value from the range ignore the values using UNDERSCORE(_) in place of a variable name. It does not provide access to the current value
let base = 3
let power = 10
var answer2 = 1
for _ in 1...power {
    answer2 *= base
}
println("\(base) to the power of \(power) is \(answer2)")

//For-in loop in Array
let names2 = ["Anna", "Alex", "Brian", "Jack"]
for name in names2 {
    println("hello, \(name)")
}
//hello, Anna
//helo, Alex
//hello, Brian
//hello, Jack

//For-in loop in Dictionary
let numberOfLegs = ["spider": 8, "ant": 6, "cat": 4]
for (animalName, legCount) in numberOfLegs { //(key, value) is decomposed and explicitly name constants (animalName, legCount)
    println("\(animalName)s have \(legCount) legs")
}

//For-in loop in String
for character in "Hello" {
    println(character)
}
//H
//e
//l
//l
//o

//-------> For-Condition-Increment

for var index = 0; index < 3; ++index {
    println("index is \(index)")
}

//retrieve final value after the loop ends declare index or variable before the loop ends
var index: Int
for index = 0; index < 3; ++index {
    println("index is \(index)")
}

println("The loop statements were executed \(index) times")

//--------> While Loops
/*
    A while loop performs a set of statement until a condition becomes false
        1. while
        2. do-while
 */

//While - a set of statement is repeated until the condition is false
/*
    while condtion {
        statements
    }

 */

//Sample: Snake and ladder
let finalSquare = 25
var board = [Int](count: finalSquare + 1, repeatedValue: 0)

//Add squares to have values, this values is the number that you'll move to the board
board[03] = +08
board[06] = +11
board[09] = +09
board[10] = +02
board[14] = -10
board[19] = -11
board[22] = -02
board[24] = -08

//Game logic
var square = 0 //Player's starting square
var diceRoll = 0
while square < finalSquare {
    //roll the dice
    if ++diceRoll == 7 { //if dice roll become too large return it to 1
        diceRoll = 1
    }
    //move by the rolled amount
    square += diceRoll
    if square < board.count {
        //if we're still on the board, move up or down for a snake or ladded
        square += board[square]
    }
}
println("Game Over")


//-------> DO-WHILE
/*
    Performs the loop block first before considering the loop's condition. Continues to repeat the loop until the condtion is false
 */

//Sample: Snake and ladder
square = 0 //reset square
diceRoll = 0 //reset diceRoll

do {
    square += board[square]
    
    //roll the dice
    if ++diceRoll == 7 {
        diceRoll = 1
    }
    //move by the rolled amount
    square += diceRoll

} while square < finalSquare
println("Game Over");

/****************************
 ***Conditional Statements***
 ****************************/

/*
        2 Types of Conditional statement
    1. If statement
    2. Switch statement
 */

//1. If

var temperatureInFahrenheit = 90
if temperatureInFahrenheit <= 32 {
    println("It's very cold. Consider wearing a scarf")
} else if temperatureInFahrenheit > 86 {
    println("It's really warm. Don't forget to wear sunscreen")
} else {
    println("It's not that cold. Wear a t-shirt")
}

//2. Switch
//Switch statement must be EXHAUSTIVE. Every possible value of the type being considered must be matched by one of the switch cases. You can catch-all case by using DEFAULT.

//e.g.
let someCharacter: Character = "e"
switch someCharacter {
case "a", "e", "i", "o", "u":
    println("\(someCharacter) is a vowel")
case "b", "c", "d", "f":
    println("\(someCharacter) is a consonant")
default:
    println("\(someCharacter) is not a vowel or a consonant")
}

//----> NO IMPLICIT FALLTHROUGH

//In swift, switch statement do not fall through the bottom of each case and to the next one by default.
//The switch statement finishes its as soon as the first matching switch case is completed without requireing explicit break statement.

//e.g
let anotherCharacter: Character = "a"
switch anotherCharacter {
case "a",
"A":
    println("The letter A")
default:
    println("Not letter A")
}

//----->RANGE MATCHING

let count = 3_000_000_000_000
let countedThings = "stars in the Milky Way"
var naturalCount: String
switch count {
case 0:
    naturalCount = "no"
case 1...3:
    naturalCount = "a few"
case 4...9:
    naturalCount = "several"
case 1000...999_999:
    naturalCount = "thousands of"
default:
    naturalCount = "millions and millions"
}
println("There are \(naturalCount) \(countedThings).")
//prints There are millions and millions of stars in the Milky Way.

//-----> TUPLES 

//Tuples can contain range or UNDERSCORE(_) to match any value.
let somePoint = (1, 1)
switch somePoint {
case (0, 0):
    println("(0, 0) is at the origin")
case (_, 0):
    println("(\(somePoint.0), 0) is on x - axis")
case (0, _):
    println("(0, \(somePoint.1)) is on the y - axis")
case (-2...2, -2...2):
    println("(\(somePoint.0), \(somePoint.1)) is inside the box")
default:
    println("(\(somePoint.0), \(somePoint.1)) is outside the box")
}
//prints (1, 1) is inside the box
//if it is set to (0, 0), it could match all four cases but since swift do not fall through the first matching cased is always used.

//-----> VALUE BINDINGS

//Switch case can bind the values it matches to temporary constants or variables
//e.g.
let anotherPoint = (2, 0)
switch anotherPoint {
case (let x, 0):
    println("on the x-axis with the x value of \(x)")
case (0, let y):
    println("on the y-axis with the y value of \(y)")
case let (x, y): //you can also use var
    println("somewhere else at (\(x), \(y))")
}
//prints "on the x-axis with an x value of 2"
//default case is not needed above because the last case accepts all value so default case is not needed.

//------>WHERE

//A switch case can use a WHERE clause to check for conditions
//e.g
let yetAnotherPoint = (1, -1)
switch yetAnotherPoint {
case let(x, y) where x == y: //acccepts this case if x == y
    println("(\(x), \(y)) is on the line x == y")
case let (x, y) where x == -y:
    println("(\(x), \(y)) is on the line x == -y")
case let (x, y):
    println("(\(x), \(y)) is just some arbitrary point")
}

/************************************************************************
                            CONTROL TRANSFER STATEMENTS
*************************************************************************/

/*
    Change the order in which your code is executed.
    
        4 Control Transfer statement
    1. continue
    2. break
    3. fallthrough
    4. return
 */

//------> CONTINUE
//Tells the loop to stop what it is doing and start again at the beginning of the iteration
//e.g.
let puzzleInput = "great minds think alike"
var puzzleOutput = ""
for character in puzzleInput {
    switch character {
    case "a", "e", "i", "o", "u":
        continue
    default:
        puzzleOutput += character
    }
}
println(puzzleOutput)
//prints grtmndsthnklk

//------> BREAK
//Break statement ends execution of entire control flow statement immediately.
//Can be used inside switch or loop

//-->Break In Loop

var bX = 0
for bX = 0; bX < 5; ++bX {
    if bX == 2 {
        break; //ends the whole for loop
    }
}
println("bx is \(bX)") //bX == 2


//--> Break in Switch
//BREAK causes the switch statement to end its execution and move the next line of code after the switch statement
//e.g.
let numberSymbol: Character = "三"
var possibleIntegerValue: Int?
switch numberSymbol {
case "1", "١", "一", "๑":
    possibleIntegerValue = 1
case "2", "٢", "二", "๒":
    possibleIntegerValue = 2
case "3", "٣", "三", "๓":
    possibleIntegerValue = 3
case "4", "٤", "四", "๔":
    possibleIntegerValue = 4
default:
    break

}

if let integerValue = possibleIntegerValue { //optional binding
    println("The integer value of \(numberSymbol) is \(integerValue)")
} else {
    println("An integer value could not be found for \(numberSymbol)")
}

//----> FALLTHROUGH
//By default, swift does not fallthrough at the bottom of swtich statements. 
//But if you really need to use fallthrough on a case-by-case basis you can use the fallthrough keyword to continue to the next default case.
//e.g.
let integerToDescribe = 5
var description = "The number \(integerToDescribe) is"
switch integerToDescribe {
case 2, 3, 5, 7, 11, 13, 17, 19:
    description += " a prime number, and also"
    fallthrough //this enables to move to the next case statement or default case vlock
default:
    description += " an integer"
}
println(description)

// prints "The number 5 is a prime number, and also an integer"

//-----> LABELED STATEMENTS

//You can nest loops and switch statements in other loops and switch statements in swift.
//you can BREAK or CONTINUE LABELED STATEMENTS

//SAMPLE; SNAKE AND LADDER - in order to win you must land exactly on square 25.
//Resuse the previous declared variable
square = 0
diceRoll = 0

gameLoop: while square != finalSquare { //gameLoop is the STATEMENT LABEL
    if ++diceRoll == 7 {
        diceRoll = 1
    }
    switch square + diceRoll {
    case finalSquare:
        break gameLoop //diceRoll will move to the final square, so the game is over. The loop is ended.
        //if the break statement did not use the the gameLoop the break statement will breakout the switch
    case let newSquare where newSquare > finalSquare:
        continue gameLoop //diceRoll will move us beyond the final square, so roll again. The loop will continue to the next iteration
    default:
        square += diceRoll
        square += board[square]
    }
}
println("Game Over")

/******************
 ****FUNCTIONS*****
 ******************/

/************************************************************************
                    DEFINING AND CALLING FUNCTIONs
*************************************************************************/

/* 
    Input - PARAMETERS
    Output - RETURN TYPE
    Passed input values that match the parameter - ARGUMENTS
 */

//e.g.
func sayHello(personName: String) -> String { //Input parameter of String value personName with return type of String using ->
    let greeting = "Hello, " + personName + "!"
    return greeting //return type of String
}
//OR
func sayHelloAgain(personName: String) -> String {
    return "Hello again, " + personName + "!"
}
println(sayHello("Anna")) //String argument value - "Anna"
println(sayHelloAgain("Anna"))

/************************************************************************
                    FUNCTION PARAMETERS AND RETURN VALUES
*************************************************************************/

//------------> MULTIPLE INPUT PARAMETER

//multiple input parameter are seperated by comma ','

//e.g. function that takes a start and an index for half-open range, and works out how many elements the range contains
func halfOpenRangeLength(start: Int, end: Int) -> Int {
    return end - start
}
println(halfOpenRangeLength(1, 10))

//------------> FUNCTION WITHOUT PARAMETERS
//e.g.
func sayHelloWorld() -> String {
    return "hello, world"
}
println(sayHelloWorld())

//------------> FUNCTION WITHOUT RETURN VALUES
//e.g.
func sayGoodBye(personName: String) {
    println("Goodbye, \(personName)")
}
println(sayGoodBye("dave"))

//Functions without a defined return type return a special value of type VOID. This is simply an empty tuple.

//The return value of a function can be ignored when it is called:
//e.g.
func printAndCount(stringToPrint: String) -> Int {
    println(stringToPrint)
    return countElements(stringToPrint)
}
func printWithoutCounting(stringToPrint: String) {
    printAndCount(stringToPrint)
}
printAndCount("hello, world") //prints "hello, world" and returns a value of 12
printWithoutCounting("hello, world")//prints "hello, world" but does not return a values


//-----------> FUNCTION WITH MULTIPLE RETURN VALUES

//You can use a tuple type as the return type for a function to return mutiple value
//e.g
func count(string: String) -> (vowels: Int, consonants: Int, others: Int) {
    var vowels = 0, consonants = 0, others = 0
    for character in string {
        switch String(character).lowercaseString {
        case "a", "e", "i", "o", "u":
            ++vowels
        case "b", "c", "d", "f", "g", "h":
            ++consonants
        default:
            ++others
        }
    }
    return (vowels, consonants, others) //returns a named tuple with "vowels", "consonants", "others".
}
let total = count("some arbitrary strings!")
println("\(total.vowels) vowels and \(total.consonants)") //the multiple return value are accessbile because it is tuple of three named Int values.


//-----------> FUNCTION PARAMETER NAMES
/*
LOCAL PARAMETER NAMES - parameter that can only be used within the body of the function.
*/

//-----------> EXTERNAL PARAMETER NAMES
/*
EXTERNAL PARAMETER NAMES - this is the name you want to give to your local parameter.

func someFunction(externalParameterName localParameterName: Int)
{
// function body goes here, and can use localParameterName
// to refer to the argument value for that parameter
}

When an external parameter is added you must used that external parameter when calling the function.
*/

func join(s1: String, s2: String, joiner: String) -> String {
    return s1 + joiner + s2
}
join("hello", "world", ",")
//return hello world

//Based on the example above it's not clear for the when you call the function
//To make the purpose of these string values clearer:
//e.g.
func join(string s1: String, toString s2: String, withJoiner joiner: String) -> String {
    return s1 + joiner + s2
}
join(string: "hello", toString: "world", withJoiner: ", ") //calling of method using external parameter names.

//------------> SHORTHAND EXTERNAL PARAMETER NAMES
/*
You can set your external parameter name by having a prefix of (#) in you parameter name. This tells swift that your setting your parameter to be both local and external paramter
*/

func containsCharacter(#string: String, #characterToFind: Character) -> Bool {
    for character in string {
        if character == characterToFind {
            return true
        }
    }
    return false
}

let containsAVee = containsCharacter(string: "aardvark", characterToFind: "v")

//-----------> DEFAULT PARAMETER VALUES

//You can define a default value for any parameter as part of a functions definition.
//e.g.
func join2(string s1: String, toString s2: String, withJoiner joiner: String = " ") -> String {
    return s1 + joiner + s2
}

join2(string: "hello", toString: "world", withJoiner: "-")
//returns "hello-world"
//if a argument is provided with the parameter with the default value then that argument value will be used. Otherwise, the default parameter value will be used.


//---------> EXTERNAL NAMES FOR PARAMETER VALUES

/*
It is useful and therefore required to provide an external name for parameters with default value.
If no external name is specified, swift autogenerates and will use the parameter name as the external parameter name
*/

//e.g.
func join3(s1: String, s2: String, joiner: String = " ") ->String {
    return s1 + joiner + s2
}
join3("hello", "world", joiner: "-") //joiner is created as an external name


//------------> VARIADIC PARAMETERS

/*
Accepts zero or more values of a specified type. You add ... at the end of the type. The variadic parameter must always appear last in the parameter list.
e.g.
Double... - this is made available within the function's body as a constant array
*/

//e.g.
func arithmethicMean(numbers: Double...) -> Double {
    var total: Double = 0
    for number in numbers { //numbers is read as an array
        total += number
    }
    return total / Double(numbers.count)
}
arithmethicMean(1, 2, 3, 4, 5)
arithmethicMean(3, 8, 19)

//------------> CONSTANT AND VARIABLE PARAMETERS

/*
Function parameters are CONSTANT by default but if you want to make that parameter mofifiable within the function's body you must use VARIABLE PARAMETER.

Variable Parameter is defined by the keyword var

*/

//-------------> Varuable Parameters
//e.g.
func alignRight(var string: String, count: Int, pad: Character) -> String {
    let amountToPad = count - countElements(string)
    for _ in 1...amountToPad {
        string = pad + string //string values is modified
    }
    return string
}
let originalString = "hello"
let paddedString = alignRight(originalString, 10, "-")
// paddedString is equal to "-----hello"
// originalString is still equal to "hello"


//------------> IN-OUT PARAMETERS
/* 
    Modifying a parameter's value after the function.
    You can only pass a variable as the argument for an in-out parameter.
*/
//e.g.
func swapTwoInts(inout a: Int, inout b: Int) {//In-out parameters uses the 'inout' keyword
    let temporaryA = a
    a = b
    b = temporaryA
}

var someInt = 3
var anotherInt = 107
swapTwoInts(&someInt, &anotherInt)
println("someInt is now \(someInt), and anotherInt is now \(anotherInt)")
//print "someInt" is now 107 and anotherInt is now "3"


/************************************************************************
                            FUNCTION TYPES
*************************************************************************/
/*
    Function Type is made up of paremeter types and return type
 */
//e.g.
func addTwoInts(a: Int, b: Int) -> Int{
    return a + b
}
func multiplyTwoInts(a: Int, b: Int) -> Int{
    return a * b
}
//function type is (Int, Int) -> Int

//-------> USING FUNCTION TYPES

//You can define a constant and variable to be of a function type and assign an appropriate function to that variable:

var mathFunction: (Int, Int) -> Int = addTwoInts //assigning the function to a variable
println("Result : \(mathFunction(2, 3))")
// prints "Result: 5"

// Assigning another function to mathFunction
mathFunction = multiplyTwoInts
println("Result : \(mathFunction(2, 3))")
// prints "Result: 6"

//By Type Inference
let anotherMathFunction = addTwoInts //This is inferred as of type function

//---------> FUNCTION TYPES AS PARAMETER TYPES

func printMathResult(mathFunction: (Int, Int) -> Int, a: Int, b: Int) {
    println("Result: \(mathFunction(a, b))")
}
printMathResult(addTwoInts, 3, 5) //addTwoInts is passed as an argument ot mathFunction parameter with (Int, Int) -> Int Function types
//print result is 8


//-----------> FUNCTION TYPE AS RETURN TYPES
//e.g.
func stepForward(input: Int) -> Int {
    return input + 1
}

func stepBackward(input: Int) -> Int { //function type of (Int) -> Int
    return input - 1
}

func chooseStepFunction(backwards: Bool) -> (Int) -> Int {
    return backwards ? stepBackward : stepForward //returns the method
}

//Using stepaction
var currentValue = 3
let moveNearerToZero = chooseStepFunction(currentValue > 0)
//move near zero now refers to stepBackwardFunction because it returns the function stepbackward with type (Int) -> Int to moveNearerTozer now accepts (Int) as param and Int as it's return type

println("Counting to zero:")
//counting to zero
while currentValue != 0 {
    println("\(currentValue)..")
    currentValue = moveNearerToZero(currentValue)
}


//---------------> NESTED FUNCTIONS
//Functions inside a function
//Hidded by the outside world

//Chooses Step Function method using nested functions

func chooseStepFunction2(backwards: Bool) -> (Int) -> Int {
    func stepForward(input: Int) -> Int {
        return input + 1
    }
    func stepBackward(input: Int) -> Int { //function type of (Int) -> Int
        return input - 1
    }
    return backwards ? stepBackward : stepForward //returns the method
}

currentValue = -4
let moveNearerToZero2 = chooseStepFunction2(currentValue > 0)
//Now refers to stepForward Function
while currentValue != 0 {
    println("\(currentValue)...")
    currentValue = moveNearerToZero2(currentValue)
}

/************************************************************************
                            CLOSURES
*************************************************************************/

/*
    Are self-contained blocks of functionality and can be passed around and used in your code. 

    Closures are blocks in objective c

        3 forms of Closure
    1. Global Functions
    2. Nested Functions
    3. Closure expressions

 */

/************************************************************************
                            CLOSURE EXPRESSIONS
*************************************************************************/

//SORT FUNCTION
//One way to provide the sorting closure is the normal function of a correct type.

let names3 = ["Chris", "Alex", "Ewa", "Barry", "Daniella"]

func backwards(s1: String, s2: String) -> Bool {
    var x = s1
    var y = s2
    return s1 > s2
}
var reversed = sorted(names3, backwards)

//-------> Closure Expression Syntax
/*
    SYNTAX:
    { (parameter) -> return type in
        statements
    }

    parameter can be constant, variable, and inout, variadic, tuples
 */

//Closure expression version

var reversed2 = sorted(names3, {(s1: String, s2: String) -> Bool in
                                    return s1 > s2})

/************************************************************************
                    INFERRING PARAMETER TYPES
*************************************************************************/

//Based on our example names, names is an array of String. Swift has an option to infer it's parameters and return type.

 //s1 and s2 are inferred as string
// the return type is inferred as boolean
reversed = sorted(names3, { s1, s2 in
                            return s1 > s2 })
//making them explicit is encouraged for code readablity.


/************************************************************************
            IMPLICIT RETURNS FROM SINGLE EXPRESSION CLOSURES
*************************************************************************/

/*
    Single expression closures can implicitly return the result of their single expression by omitting the 'return' keyword
 */

reversed = sorted(names3, {s1, s2 in s1 > s2 }) //the return is removed from the inferre closure expression.


/************************************************************************
                    SHORTHAND ARGUMENT NAMES
*************************************************************************/

/*
    Shorthand argument for Swift closure:
        $0, $1, $2
 */

reversed = sorted(names3, { $0 > $1 }) //$0 and $1 are the closures first and second string argumen


/************************************************************************
                        OPERATOR FUNCTIONS
*************************************************************************/

reversed = sorted(names3, >) // > this is being interpreted as a function with two string type parameters that returns a value of type bool.


/************************************************************************
                        TRAILING CLOSURES
*************************************************************************/

/*
    Trailing closure is used when you need to pass a closure expression to a function's final argument and the closure is long.

    Trailing closure is outside of and after the parenthesis of the function call it supports
 */
//e.g.
func someFunctionThatTakesAClosure(closure: () -> ()) {
    //function body goes here
}

//Here's how you call a function without a trailing closure
someFunctionThatTakesAClosure({
    
    })

//Here's how you call this function with a trailing closure
someFunctionThatTakesAClosure() {
    //trailing closure's body goes here
}

//Using trailing closure 
reversed = sorted(names) {
    $0 > $1
}

//Closure are most useful when the closure is sufficiently long that it is not possible to write in inline on a single line.

//e.g.
//Converting array of Int Value to Array of String values
let digitNames = [0: "Zero", 1: "One", 2: "Two", 3: "Three", 4: "Four", 5: "Five", 6: "Six", 7: "Seven", 8: "Eight", 9: "Nine", 10: "Ten"]
let numbers = [16, 58, 510]

let strings = numbers.map { //inferred of String Array because of the return type.
    (var number) -> String in
    var output = ""
    number
    while number > 0 {
        output = digitNames[number % 10]! + output
        //must be unwrapped, because dict subscript returns an optional value to indicate that dictionary can fail if the key does not exist.
        number /= 10
    }
    return output
}
16%10
16/10
1%10


/************************************************************************
                            CAPTURING VALUES
*************************************************************************/

/*
    Closure can capture constants and variabless from the surrounding context in which it is defined.
 */
//e.g.
func makeIncrementor (forIncrement amount: Int) -> () -> Int {
    var runningTotal = 0
    func incrementor() -> Int {
        runningTotal += amount
        return runningTotal
    }
    return incrementor
}

let incrementByTen = makeIncrementor(forIncrement: 10)
incrementByTen()
incrementByTen()
incrementByTen()
//just adds amount to runningTotal. It captures the existing values outside it's scope.
//because it modifies the the current running total variable, incrementor captures a REFERENCE to the current runningTotal variable and not just a copy of its initial value.

let incrementBySeven = makeIncrementor(forIncrement: 7) //this will create a new store reference
incrementBySeven()
incrementBySeven()
incrementBySeven()

/************************************************************************
                            CLOSURES ARE REFERENCE TYPES
*************************************************************************/

/*
    Functions and closure are reference types
 */


/*#######################################################################
                            ENUMERATIONS
########################################################################*/

/************************************************************************
                            ENUMERATION SYNTAX
*************************************************************************/

//SYNTAX:
enum SomeEnumeration {
    // enumeration definition goes here
}

//e.g
enum CompassPoint {
    case North
    case South
    case East
    case West
}

//North, South, East, West are the MEMBER VALUES
//Enumeration are not assign a default integer value

//Multiple member values can appear on a single line
enum Planet {
    case Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune
}

//Give enumeration types singular and starts with a captital letter, so they read as self evident.

var directionToHead = CompassPoint.West
//once directionToHead is declared as compassPoint, you can set it to different compassPoint using shorter dot syntax
directionToHead = .East

/************************************************************************
            MATCHING ENUMERATION VALUES with a SWITCH STATEMENT
*************************************************************************/

directionToHead = .South
switch directionToHead {
case .North:
    println("Lots of planets have a north")
case .South:
    println("Watch out for penguins")
case .East:
    println("Where the sun rises")
case .West:
    println("Where the skies are blue")
}
//prints watch out for penguins
//switch statement must be exhaustive when considering enumertion numbers. It's either you put all the cases for the member values or add a default to catch an omitter or else the code will not run. If the case of .West is omitted it will not compile.


//When it is not appropriate to provide a case for every enum members you can provide a default case to cover any members
//e.g.
let somePlanet = Planet.Earth
switch somePlanet {
case .Earth:
    println("Mostly harmless")
default:
    println("Not a safe place for humans")
}
//println "Mostly harmless"


/************************************************************************
                                ASSOCIATED VALUES
*************************************************************************/

//Defining enum members of associated values with different types
//e.g.
enum Barcode {
    case UPCA(Int, Int, Int) //UPCA speicific barcode
    case QRCode(String)     //QR specific barcode
}

//New barcodes can then be created using either type
var productBarcode = Barcode.UPCA(8, 85909-51226, 3)
productBarcode = Barcode.QRCode("ABSDEFGHIJKLMNOP")

//Different barcode types can be checked using a switch statement
switch productBarcode {
case .UPCA(let numberSystem, let identifier, let check):
    println("UPC-A with value of \(numberSystem), \(identifier), \(check).")
case .QRCode(let productCode):
    println("QR code with value of \(productCode).")
}
//print "QR code with the value ABCDEFGHIJKLMNOP"

//If all are extracted as contants or variables you can place a single var or let annotation before the member name for brevity:
//e.g.
switch productBarcode {
case let .UPCA(numberSystem, identifier, check):
    println("UPC-A with value of \(numberSystem), \(identifier), \(check).")
case let .QRCode(productCode):
    println("QR code with value of \(productCode).")
}
//print "QR code with the value ABCDEFGHIJKLMNOP"

/************************************************************************
                                RAW VALUES
*************************************************************************/

//Defining enum member of raw values with default values and same type
//e.g.
enum ASCIIIControlCharacter: Character {
    case Tab = "\t"
    case LineFeed = "\n"
    case CarriageReturn = "\r"
}
//defined of type Character with raw values "\t", "\n"

//Planet enumeration represent Planet's order from the sun
enum Planet2: Int {
    case Mercury = 1, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune
}

//Accessing raw value of a member
let earthsOrder = Planet2.Earth.toRaw()

let possiblePlane = Planet2.fromRaw(7)
// possiblePlanet is of type Planet? and equals Planet.Uranus
// will return either nil or have a value because it's return type is optional
//e.g if fromRaw(9) will return nil since it is not included in the case

let positionToFind = 9
if let somePlanet = Planet2.fromRaw(positionToFind) {
    switch somePlanet {
    case .Earth:
        println("Most harmless")
    default:
        println("Not a safe place for humans")
        
    }
} else {
    println("There isn't a planet at position \(positionToFind)")
}
// prints "There isn't a planet at position 9". Planet2.fromRaw(positionToFind) returns nil because there is no position 9 in the Planet.
