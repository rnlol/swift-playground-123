// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/*#######################################################################
                        OPTIONAL CHAINING
########################################################################*/

/*
    Optional chaining is a process for querying and calling properties, methods, and subscripts on an optional that might currently be nil. If the optional contains a value, the property, method, or subscript call succeeds; if the optional is nil, the property, method, or subscript call returns nil.
 */


/************************************************************************
        Optional Chaining as an Alternative to Forced Unwrapping
*************************************************************************/

//e.g.
class Person1 {
    var residence: Residence1?
}

class Residence1 {
    var numberOfRooms = 1
}

//Residence have a single Int property called numberOfRooms = 1
//Person has an optional residence property of type Residence?

let john1 = Person1()
//the residence is default initialized to nil by virtue of being optional

//let roomCount = john1.residence!.numberOfRooms
//this trigger a runtime error because it's nil

//To use optional chaining, use a question mark in place of the exclamation mark
if let roomCount = john1.residence?.numberOfRooms { //optional chaining attempts return s a value type Int?
    println("John's residence has \(roomCount) rooms")
} else {
    println("Unable to retrieve the number of rooms")
}
// prints unable to retrieve the number of rooms
// when residence is nil the optional int will also be nil

//You can assign a Residence instance to john.residence, so that it no longer has a nil value:
john1.residence = Residence1()

if let roomCount = john1.residence?.numberOfRooms {
    println("John's residence has \(roomCount) room(s)")
} else {
    println("Unable to retrieve the number of rooms.")
}


/************************************************************************
            Defining Model Classes for Optional Chaining
*************************************************************************/


class Person {
    var residence: Residence?
}

class Residence {
    var rooms = [Room]()
    var numberOfRooms: Int { //computed property
    return rooms.count
    }
    subscript(i: Int) -> Room {
        return rooms[i]
    }
    func printNumberOfRooms() {
        println("The number of rooms is \(numberOfRooms)")
    }
    var address: Address?
}

class Room {
    let name: String
    init(name: String) {
        self.name = name
    }
}

class Address {
    var buildingName: String?
    var buildingNumber: String?
    var street: String?
    func buildingIdentifier() -> String? {
        if buildingName != nil {
            return buildingName
        } else if buildingNumber != nil{
            return buildingNumber
        } else {
            return nil
        }
    }
}

/************************************************************************
            Calling Properties Through Optional Chaining
*************************************************************************/

let john = Person()
if let roomCount = john.residence?.numberOfRooms {
    println("John's residence has \(roomCount) room(s)")
} else {
    println("Unable to retrieve the number of rooms.")
}
// prints "Unable to retrieve the number of rooms"
// because john.residence is nil, the optional chaining calls fails but without error


/************************************************************************
        Calling Methods Through Optional Chaining
*************************************************************************/

if john.residence?.printNumberOfRooms() != nil { //this will return void? void - If the method was called successfully, nil if not
    println("It was possible to print the number of rooms")
} else {
    println("It was not possible to print the number of rooms")
}

/************************************************************************
        Accessing Subscripts Through Optional Chaining
*************************************************************************/

if let firstRoomName = john.residence?[0].name { //returns optional Room?, returns Room or nil
    println("The first room is \(firstRoomName).")
} else {
    println("Unable to retrieve the first room name.")
}
// prints "Unable to retrieve the first room name"

let johnsHouse = Residence()
johnsHouse.rooms.append(Room(name: "Living Room")) //must be enclosed in [] or used append for arrays
johnsHouse.rooms.append(Room(name: "Kitchen"))
john.residence = johnsHouse

if let firstRoomName = john.residence?[0].name {
    println("The first room name is \(firstRoomName)")
} else {
    println("Unable to retrieve the first room name")
}
// prints "The first roomname is Living Room."

/************************************************************************
            Linking Multiple Levels of Chaining
*************************************************************************/

/*
    1. If the type you are trying to retrieve is not optional, it will become optional because of the optional chaining
    2. If the type you are trying to retrieve is already an optional,it will not become more optional because of the chaining.
 */

if let johnStreet = john.residence?.address?.street { //The value of address is nil
    println("John's street name is \(johnStreet).")
} else {
    println("Unable to retrieve the address.")
}
// prints  "Unable to retrieve the address"

//Assign an address to residence
let johnsAddress = Address()
johnsAddress.buildingName = "The Larches"
johnsAddress.street = "Laurel Street"
john.residence!.address = johnsAddress

if let johnStreet = john.residence?.address?.street {
    println("John's street name is \(johnStreet).")
} else {
    println("Unable to retrieve the address.")
}
// prints "John's street name is Laurel Street"

/************************************************************************
        Chaining on Methods With Optional Return Values
*************************************************************************/

//e.g.
if let buildingIdentifier = john.residence?.address?.buildingIdentifier() { //Returns a String?
    println("John's building identifier is \(buildingIdentifier).")
}
// prints "John's building identifier is The Larches."

if john.residence?.address?.buildingIdentifier()?.hasPrefix("The") == true {
    println("John's building identifier begins with \"The\".")
}
// prints "John's building identifier begins with "The"."

/*#######################################################################
                        TYPE CASTING
########################################################################*/

/*
    Typecasting is a way to check the type of an instance.
 */

/************************************************************************
            Defining a Class Hierarchy for Type Casting
*************************************************************************/

//e.g
//Bas class
class MediaItem {
    var name: String
    init(name: String) {
        self.name = name
    }
}

//Subclass
class Movie: MediaItem {
    var director: String
    init(name: String, director: String) {
        self.director = director
        super.init(name: name)
    }
}

class Song: MediaItem {
    var artist: String
    init(name: String, artist: String) {
        self.artist = artist
        super.init(name: name)
    }
}

//Array that contains 2 movie instances and 3 songs
let library = [
    Movie(name: "Casablanca", director: "Michael Curtiz"),
    Song(name: "Blue Suede Shoes", artist: "Elvis Presley"),
    Movie(name: "Citizen Kane", director: "Orson Welles"),
    Song(name: "The One and Only", artist: "Chesney Hawkes"),
    Song(name: "Never Gonna Give You Up", artist: "Rick Astley")
]
//the type of library is inferred to be media item

/************************************************************************
            Checking Type
*************************************************************************/

/*
    Use the type check operator(is) to check whether an instance is of certain subclass type.
 */

var movieCount = 0
var songCount = 0

for item in library {
    if item is Movie { //Type checking for Movie
        ++movieCount
    } else if item is Song { //Type checking for Song
        ++songCount
    }
}

println("Media library contains \(movieCount) movies and \(songCount) songs")


/************************************************************************
                Downcasting
*************************************************************************/

/*
    A constant or variable of a certain class type may actually refer to an instance of a subclass behind the scenes
    
    You can try to downcast to the subclass type with the type cast operator (as)

    Use the optional form of the type cast operator (as?) when you are not sure if the downcast will succeed.

    Use the forced form of the type cast operator (as) only when you are sure that the downcast will always succeed
 */

for item in library {
    if let movie = item as? Movie { // uses optional as? because where not sure if the item is a Movie or a song
        println("Movie: '\(movie.name)', dir: '\(movie.director)'")
        movie.name
    } else if let song = item as? Song {
        println("Song: '\(song.name)', by \(song.artist)")
        song.name
    }
}

/************************************************************************
            Type Casting for Any and AnyObject
*************************************************************************/

/*
    Swift provides two special type aliases for working with non-specific types:
    
    1. AnyObject can represent an instance of any class type
    2. Any can represent an instance of any type at all, apart from function types

Use Any and AnyObject only when you explicitly need the behavior and capabilities they provide. It is always better to be specific about the types you expect to work with in your code.


 */

/************************************************************************
            AnyObject
*************************************************************************/

/*
    You can use the forced version of the type cast operator (as) to downcast each item in the array to a more specific class type than AnyObject, without the need for optional unwrapping
 */

let someObjects: [AnyObject] = [
    Movie(name: "2001: A Space Odyssey", director: "Stanley Kubrick"),
    Movie(name: "Moon", director: "Duncan Jones"),
    Movie(name: "Alien", director: "Ridley Scott")
]

//Because this array is known to contain only Movie instances, you can downcast and unwrap directly to a non optional movie with the forced version of the type cast operator(as)

for object in someObjects {
    let movie = object as Movie
    println("Movie: '\(movie.name)', dir. \(movie.director)")
}

/************************************************************************
                    Any
*************************************************************************/

/*
    Example of Any to work with a mix of different types, including non-class types.
 */

var things = [Any]()
things.append(0)
things.append(0.0)
things.append(42)
things.append(3.14159)
things.append("hello")
things.append((3.0, 5.0))
things.append(Movie(name: "Ghostbusters", director: "Ivan Reitman"))

for thing in things {
    switch thing {
    case 0 as Int:
        println("zero as int")
    case 0 as Double:
        println("Zero as a Double")
    case let someInt as Int:
        println("an integer value of \(someInt)")
    case let someDouble as Double where someDouble > 0:
        println("a positive double value of \(someDouble)")
    case is Double:
        println("some other double value that I don't want to print")
    case let someString as String:
        println("a string value of \"\(someString)\"")
    case let (x, y) as (Double, Double):
        println("an (x, y) point at \(x), \(y)")
    case let movie as Movie:
        println("a movie called '\(movie.name)', dir. \(movie.director)")
    default:
        println("something else")
    }
}


/*#######################################################################
                            NESTED TYPES
########################################################################*/

//e.g. BLACK JACK CARD
struct BlackjackCard {
    
    //nested Suit Enumeration
    enum Suit: Character {
        case Spades = "♠", Hearts = "♡", Diamonds = "♢", Clubs = "♣"
    }
    
    // nested Rank enumeration
    enum Rank: Int {
        case Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten
        case Jack, Queen, King, Ace
        struct Values { //struct properties that will be used to return the value
            let first: Int, second: Int?
        }
        var values: Values { //computed property that return an instance of the Values structure
        switch self {
        case .Ace:
            return Values(first: 1, second: 11)
        case .Jack, .Queen, .King:
            return Values(first: 10, second: nil)
        default:
            return Values(first: self.toRaw(), second: nil)
            }
        }
    }
    
    //Blackjackcard properties and methods
    let rank: Rank, suit: Suit
    var description: String { //computed property
    var output = "suit is \(suit.toRaw())"
        output += " value is \(rank.values.first)"
        if let second = rank.values.second {
            output += " or \(second)"
        }
        return output
    }
}

let theAceOfSpades = BlackjackCard(rank: .Ace, suit: .Spades)
println("theAceOfSpades: \(theAceOfSpades.description)")
theAceOfSpades.description

//To use a nested type outside of its definition context, prefix its name with the name of the type it is nested within:
let heartsSymbol = BlackjackCard.Suit.Hearts.toRaw()
heartsSymbol


/*#######################################################################
                           EXTENSIONS
########################################################################*/


/*
    Extensions add new functionality to an existing class, structure, or enumeration type.

    Extensions in Swift can:

    * Add computed properties and computed static properties
    * Define instance methods and type methods
    * Provide new initializers
    * Define subscripts
    * Define and use new nested types
    * Make an existing type conform to a protocol

 */


/************************************************************************
                        Extension Syntax
*************************************************************************/


/*
    extension SomeType {
        // new functionality to add to Sometype goes here
    }

    extension SomeType: SomeProtocol, AnotherProtocol {
        // implementation of protocol requirement here
    }
*/

/************************************************************************
                        Computed Properties
*************************************************************************/

/*
    Extensions can add computed instance properties and computed type properties to existing types.
 */
//e.g. //extension Double returns computed properties
extension Double {
    var km: Double {
    return self * 1_0000.0
    }
    var m: Double {
    return self
    }
    var cm: Double {
    return self / 100.0
    }
    var mm: Double {
    return self / 1_000.0
    }
    var ft: Double {
    return self / 3.28084
    }
}

let oneInch = 25.4.mm //name of properties can be appended
println("one inch is \(oneInch) meters")

let aMarathon = 42.km + 195.m
// prints "A marathon is 42195.0 meters long"

/************************************************************************
                        Initializers
*************************************************************************/

/*
    Extensions can add new initializers to existing types.

    Extensions can add new convenience initializers to a class, but they cannot add new designated initializers or deinitializers to a class

    Designated initializers and deinitializers must always be provided by the original class implementation.
 */

struct Size {
    var width = 0.0, height = 0.0
}
struct Point {
    var x = 0.0, y = 0.0
}
struct Rect {
    var origin = Point()
    var size = Size()
}

//because Rect structure provides default values for all its properties it receives a default initializer and a memberwise initializer automatically.

let defaultRect = Rect()
let memberwiseRect = Rect(origin: Point(x: 2.0, y: 2.0), size: Size(width: 5.0, height: 5.0)) //Because point and

extension Rect {
    init(center: Point, size: Size) { //new initializer is created in the extension
        let originX = center.x - (size.width / 2)
        let originY = center.y - (size.height / 2)
        self.init(origin: Point(x: originX, y: originY), size: size)
    }
}

//If you provide a new initializer with an extension, you are still responsible for making sure that each instance is fully initialized once the initializer completes.
let centerRect = Rect(center: Point(x: 4.0, y: 4.0), size: Size(width: 3.0, height: 3.0))


/************************************************************************
                            METHODS
*************************************************************************/

/*
    Extensions can add new instance methods and type methods to existing types
 */
extension Int {
    func repetitions(task: () -> ()) {
        for i in 0..<self {
            task()
        }
    }
}

//Call the method using  closure
3.repetitions({
    println("Hello!") //this is the argument to the method repetition
    })

//trailing closure syntax -> check closure for review
3.repetitions {
    println("Goodbye!")
}

/************************************************************************
                            MUTATING INSTANCE METHODS
*************************************************************************/

/*
    Instance methods added with an extension can also modify (or mutate) the instance itself.

    Structure and enumeration that modify self or its properties must mark the instance method as mutating, just like mutating methods from an original implementation.
 */

extension Int {
    mutating func square() {
        self = self * self
    }
}
var someInt = 3
someInt.square()
someInt
// someInt is now 9


/************************************************************************
                        SUBSCRIPTS
*************************************************************************/

/*
    Extensions can add new subscripts to an existing type.
 */

extension Int {
    subscript(var digitIndex: Int) -> Int {
        var decimalBase = 1
            while digitIndex > 0 {
                decimalBase *= 10
                --digitIndex
            }
            return (self / decimalBase) % 10
    }
}

746381295[0]
// returns 5
746381295[1]
// returns 9
746381295[2]
// returns 2
746381295[8]
// returns 7

/************************************************************************
                    NESTED TYPES
*************************************************************************/

extension Int {
    enum Kind {
        case Negative, Zero, Positive
    }
    //kind returns enum values Negative, Positive and Zero
    var kind: Kind {
    switch self {
    case 0:
        return .Zero
    case let x where x > 0:
        return .Positive
    default:
        return .Negative
        }
    }
}

func printIntegerKinds(numbers: [Int]) {
    for number in numbers {
        switch number.kind {
        case .Negative:
            print("- ")
        case .Zero:
            print("0 ")
        case .Positive:
            print("+ ")
        }
    }
}

printIntegerKinds([3, 19, -27, 0, -6, 0, 7])
// prints "+ + - 0 - 0 +"

/*#######################################################################
                        PROTOCOLS
########################################################################*/

/*
    A PROTOCOL defines a blueprint of methods, properties, and other requirement that suit a particular task or piece of functionality.

    the PROTOCOL doesn't provide any implementation , it only describe what an implementation will look like.

    the PROTOCOL can be adopted by a class, structure, or enumeration.
 */

/************************************************************************
PROTOCOL SYNTAX
*************************************************************************/
/*
    protocol SomeProtocol {
        // protocol definition goes here
    }

    struct SomeStructure: FirstProtocol, AnotherProtocol {
        // structure definition goes here
    }
    
    if class has a superclass, superclass first before protocol
    class SomeClass: SomeSuperclass, FirstProtocol, AnotherProtocol {
        // class definition here
    }
 */


/************************************************************************
PROPERTY REQUIREMENTS
*************************************************************************/

/*
* Can be instance property or type property

* Only specifies the required property name and type

* Protocol also specifies whether each property must be gettable and settable

* Property requirements are always declared as variable properties, prefixed with the 'var' keyword.

* gettable and settable properties are indicated by writing { get set } after their type declaration.
*/

//e.g.
protocol SomeProtocol {
    var mustBeSettable: Int { get set }
    var doesNotNeedToBeSettable: Int { get }
}

//Type Property
protocol AnotherProtocol {
    class var someTypeProperty: Int { get set } //always prefix type property requirement with the 'class' keyword. Even if type property requirements are prefixed with static keyword for struc and enum
}

//e.g. Protocol with a single instance property
protocol FullyNamed {
    var fullName: String { get }
}
//This protocol specifies that the thing must be able to provide a full name for itself.

//E.g. Example of struct that conforms to FullNamed Protocol
struct ProtPerson: FullyNamed {
    var fullName: String
}
let protJohn = ProtPerson(fullName: "John AppleSeed")
protJohn.fullName
// Class enum and struct must comply to the protocol

//A more complex code
class Starship: FullyNamed {
    var prefix: String?
    var name: String
    init(name: String, prefix: String? = nil) {
        self.name = name
        self.prefix = prefix
    }
    var fullName: String {
    return (prefix != nil ? prefix! + " ": "") + name
    }
}

var ncc1701 = Starship(name: "Enterprise", prefix: "USS")
ncc1701.fullName

/************************************************************************
Method Requirements
*************************************************************************/

/*
Protocols can require specific instance methods and type methods to be implemented by conforming types.

Protocol use the same syntax as normal methods, but are not allowed to specify default values for methods.
*/
//Type Methods
protocol SomeProtocolMethod {
    class func smeTypeMethod()
}

//Instance Methods
protocol RandomNumberGenerator {
    func random() -> Double
}

//e.g. Sample Implementation
class LinearCongruentialGenerator: RandomNumberGenerator {
    var lastRandom = 42.0 //default property values
    let m = 139968.0
    let a = 3877.0
    let c = 29573.0
    func random() -> Double {
        lastRandom = ((lastRandom * a + c) % m)
        return lastRandom / m
    }
}

let generator = LinearCongruentialGenerator()
println("Here's a random number: \(generator.random())")
generator.random()
println("And another oner: \(generator.random())")
generator.random()


/************************************************************************
Mutating Method Requirements
*************************************************************************/

/*
For instance methods on value types(struct and enum) you place a 'mutating' keyword before a method's func keyword
*/

//e.g
protocol Togglable {
    mutating func toggle()
}

enum OnOffSwitch: Togglable {
    case Off, On
    mutating func toggle() {
        switch self {
        case Off:
            self = On
        case On:
            self = Off
        }
    }
}
var lightSwitch = OnOffSwitch.Off
lightSwitch.toggle()
lightSwitch
// lightswitcg is now equal to .On

/************************************************************************
PROTOCOL AS TYPES
*************************************************************************/

/*
Any protocol you create will become a fully-fledged type for use in your code.
*/

//e.g. Example of Protocol used as a type
class Dice {
    let sides: Int
    let generator: RandomNumberGenerator //the protocol has be set as a type. You can set any instance that adopts the RandomNumberProtocol
    init(sides: Int, generator: RandomNumberGenerator) {
        self.sides = sides
        self.generator = generator
    }
    func roll() -> Int {
        return Int(generator.random() * Double(sides)) + 1
    }
}

//implementation
var d6 = Dice(sides: 6, generator: LinearCongruentialGenerator())
for _ in 1...5 {
    println("Random dice roll is \(d6.roll())")
    d6.roll()
}
//The LinearCongruentialGenerator class is being passed as an argument because it adopts the RandomNumberGenerator protocol.

/************************************************************************
DELEGATION
*************************************************************************/

/*
Delegation is a design pattern that enables a class or structure to hand off (or delegate) some of it's responsibilities to an instance or another type.
*/

//e.g DICE GAME
protocol DiceGame {
    var dice: Dice { get }
    func play()
}
protocol DiceGameDelegate {
    func gameDidStart(game: DiceGame)
    func game(game: DiceGame, didStartNewTurnWithDiceRoll diceRoll: Int)
    func gameDidEnd(game: DiceGame)
}

//e.g SNAKE AND LADDERS
class SnakesAndLadders: DiceGame {
    let finalSquare = 25
    let dice = Dice(sides: 6, generator: LinearCongruentialGenerator())
    var square = 0
    var board: [Int]
    init() {
        board = [Int](count: finalSquare + 1, repeatedValue: 0)
        board[03] = +08; board[06] = +11; board[09] = +09; board[10] = +02
        board[14] = -10; board[19] = -11; board[22] = -02; board[24] = -08
    }
    var delegate: DiceGameDelegate? //set the delegate
    func play() {
        square = 0
        delegate?.gameDidStart(self) //optional chaining - delegate return delegate? or nil
        gameLoop: while square != finalSquare { //labeled statement
            let diceRoll = dice.roll()
            delegate?.game(self, didStartNewTurnWithDiceRoll: diceRoll)
            switch square + diceRoll {
            case finalSquare:
                break gameLoop
            case let newSquare where newSquare > finalSquare:
                continue gameLoop
            default:
                square += diceRoll
                square += board[square]
            }
        }
        delegate?.gameDidEnd(self) //delegate instance will run this implementation in their class
    }
}

// Implementation of Delegate methods
// These are the methods that will run inside your SnakeAndLadders class
class DiceGameTracker: DiceGameDelegate {
    var numberOfTurns = 0
    func gameDidStart(game: DiceGame) {
        numberOfTurns = 0
        if game is SnakesAndLadders {
            println("Started a new game of Snakes and Ladders")
        }
        println("The game is using a \(game.dice.sides)-sided dice")
    }
    func game(game: DiceGame, didStartNewTurnWithDiceRoll diceRoll: Int) {
        ++numberOfTurns
        println("Rolled a \(diceRoll)")
    }
    func gameDidEnd(game: DiceGame) {
        println("The game lasted for \(numberOfTurns) turns")
    }
}

//DiceGameTracker
let tracker = DiceGameTracker()
let game = SnakesAndLadders()
game.delegate = tracker
game.play()

/************************************************************************
Adding Protocol Conformance with an Extension
*************************************************************************/

/*
You can extend an existing type to adopt and conform to a new protocol, even if you do not have access to the source code for the existing type

Extensions can add new properties, methods, and subscripts to an existing type, and are therefore able to add any requirements that a protocol may demand

Existing instances of a type automatically adopt and conform to a protocol when that conformance is added to the instance’s type in an extension.
*/

//e.g
protocol TextRepresentable {
    func asText() -> String
}
//Dice class is extended
extension Dice: TextRepresentable {
    func asText() -> String {
        return "A \(sides)-sided dice"
    }
}
//implementation
let d12 = Dice(sides: 12, generator: LinearCongruentialGenerator())
println(d12.asText())
// prints "A 12-sided dice"
d12.asText()

extension SnakesAndLadders: TextRepresentable {
    func asText() -> String {
        return "A game of snakes and ladders with \(finalSquare) squares"
    }
}
game.asText()

/************************************************************************
Declaring Protocol Adoption with an Extension
*************************************************************************/

/*
If a type already conforms to all of the requirements of a protocol, but has not yet stated that it adopts that protocol, you can make it adopt the protocol with an empty extension
*/

//This struct already conforms to the protocol TextRepresentable
struct Hamster {
    var name: String
    func asText() -> String {
        return "A hamster named \(name)"
    }
}

//To adopt the existing protocol, just use this:
extension Hamster: TextRepresentable {}
//instances of Hamster can now be used wherever TextRepresentable is the required type
let simonTheHamster = Hamster(name: "Simon")
let somethingTextRepresentable: TextRepresentable = simonTheHamster // the constant somethingTextRepresentable is interpreted as "this constant need be assigned with a type that conforms to TextRepresentable protocol"
println(somethingTextRepresentable.asText())
somethingTextRepresentable.asText()


/************************************************************************
COLLECTIONS of PROTOCOL TYPES
*************************************************************************/

/*
A protocol can be used as the type to be stored in a collection such as an array or a dictionary
*/

//e.g.
let thingss: [TextRepresentable] = [game, d12, simonTheHamster]
for thing in thingss { // thing constant is of type TextRepresentable. It is not of type Dice, DiceGame, or Hamster
    println(thing.asText())
    thing.asText()
}

/************************************************************************
Protocol Inheritance
*************************************************************************/

/*
A protocol can inherit one or more other protocols and can add further requirements on top of the requirement it inherits
*/

protocol InheritingProtocol: SomeProtocol, AnotherProtocol {
    //protocol definition here
}

//e.g. All that adopts PrettyTextRepresentable must satisfy all of the requirements enforced by TextRepresentable
protocol PrettyTextRepresentable: TextRepresentable {
    func asPrettyText() -> String
}

//e.g. SnakeAndLadders
///This extension states that it adopts the PrettyTextRepresentable protocol and provides an implementation of the asPrettyText method for the SnakesAndLadders type
extension SnakesAndLadders: PrettyTextRepresentable {
    func asPrettyText() -> String {
        var output = asText() + ":\n"
        for index in 1...finalSquare {
            switch board[index] {
            case let ladder where ladder > 0:
                output += "▲ "
            case let snake where snake < 0:
                output += "▼ "
            default:
                output += "○ "
            }
        }
        return output
    }
}

/************************************************************************
Protocol Composition
*************************************************************************/

/*
Protocol compositions have the form protocol<SomeProtocol, AnotherProtocol>
*/

//e.g.
protocol Named {
    var name: String { get }
}
protocol Aged {
    var age: Int { get }
}
struct Person4: Named, Aged { //mulitple protocol
    var name: String
    var age: Int
}
func wishHappyBirthday(celebrator: protocol<Named, Aged>) { //protocol composition. Multiple protocol is combined into a single requirement
    println("Happy Birthday \(celebrator.name) - you're \(celebrator.age)!")
}
let birthdayPerson = Person4(name: "Malcolm", age: 21)
wishHappyBirthday(birthdayPerson) //wish happy birthday accepts the struct as an argment that conforms to Named and Aged protocol

/************************************************************************
Checking for Protocol Conformance
*************************************************************************/

/*
    You can use 'is' and 'as' operators described in Type Casting to check for protocol conformance, and to cast to a specific protocol.

    * The is operator returns true if an instanc conforms to a protocol
    * the as? version of the downcast operator returns an optional value of the protocol's type, and this value is nil if the instance does not conform to the protocol
    * The as version of the downcast operator forces the downcast to the protocol type and triggers a runtime error if the downcast does not succeed.
 */

//e.g.
@objc protocol HasArea {
    var area: Double { get }
}
//You can check for protocol conformance only if your protocol is marked with @objc attribute.
//@objc protocols can be adopted only by classes.

class Circle: HasArea {
    let pi = 3.1415927
    var radius: Double
    var area: Double { //comply with the HasArea Protocol
    return pi * radius * radius
    }
    init(radius: Double) {
        self.radius = radius
    }
}
class Country: HasArea {
    var area: Double //comply with the HasArea Protocol
    init(area: Double) {
        self.area = area
    }
}

//A class which does not conform to the HasArea protocol
class Animal {
    var legs: Int
    init(legs: Int) {
        self.legs = legs
    }
}
//Circle, Country, and Animal do not have a shared base class so all three types can be used to initialize an array of stored values to type AnyObject
let objects: [AnyObject] = [
    Circle(radius: 2.0),
    Country(area: 243_610),
    Animal(legs: 4)
]

//objects array can now be checked to see if each object conforms to HasArea protocol
for object in objects {
    if let objectWithArea = object as? HasArea { //check if object conforms to the protocol and by as? operator in unwrapped in optional binding into a constanct
        println("Area is \(objectWithArea.area)")
    } else {
        println("Something that doesn't have an area")
    }
}

/************************************************************************
Optional Protocol Requirements
*************************************************************************/

/*
     Optional requirements are prefixed by the optional modifier as part of the protocol’s definition

 */

//e.g.
@objc protocol CounterDataSource {
    optional func incrementForCount(count: Int) -> Int //optional method requirement
    optional var fixedIncrement: Int { get } //optional property requirement
}

@objc class Counter {
    var count = 0
    var dataSource: CounterDataSource? //protocol type
    func increment() {
        if let amount = dataSource?.incrementForCount?(count) {
            count += amount
        } else if let amount = dataSource?.fixedIncrement? {
            count += amount
        }
    }
}

//Data source class
class ThreeSource: CounterDataSource { //passed to the var that has type of CounterDataSource
    let fixedIncrement = 3
}

//Implementation
var counter = Counter() //new counter 
counter.dataSource = ThreeSource() //data source
for _ in 1...4 {
    counter.increment()
    println(counter.count)
    counter.count
}

//Complex Data source
class TowardsZeroSource: CounterDataSource {
    func incrementForCount(count: Int) -> Int { //implement optional method
        if count == 0 {
            return 0
        } else if count < 0 {
            return 1
        } else {
            return -1
        }
    }
}

//e.g.
counter.count = -4
counter.dataSource = TowardsZeroSource()
for _ in 1...5 {
    counter.increment()
    println(counter.count)
}
